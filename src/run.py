import sys
from component import GoogleAdwordsComponent

sys.tracebacklimit = 0

if __name__ == '__main__':
    c = GoogleAdwordsComponent()
    c.run()
