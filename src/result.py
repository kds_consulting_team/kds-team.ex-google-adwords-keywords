import csv
import json
import os

KEYWORDS_FIELDS = ['KEYWORD_TEXT', 'COMPETITION', 'EXTRACTED_FROM_WEBPAGE', 'IDEA_TYPE',
                   'SEARCH_VOLUME', 'AVERAGE_CPC', 'REQUEST_TYPE']
KEYWORDS_PK = ['KEYWORD_TEXT']

KEYWORDS_STATS_FIELDS = ['KEYWORD_TEXT', 'YEAR', 'MONTH', 'COUNT']
KEYWORDS_STATS_PK = ['KEYWORD_TEXT', 'YEAR', 'MONTH']


class GoogleAdwordsWriter:

    KEYWORDS_FILE_NAME = 'keywords.csv'
    KEYWORDS_STATS_FILE_NAME = 'keywords-stats.csv'

    def __init__(self, table_path, incremental):

        _keywords_path = os.path.join(table_path, self.KEYWORDS_FILE_NAME)
        _stats_path = os.path.join(table_path, self.KEYWORDS_STATS_FILE_NAME)

        self.keywords = csv.DictWriter(open(_keywords_path, 'w'), fieldnames=KEYWORDS_FIELDS,
                                       restval='', extrasaction='ignore', quotechar='\"',
                                       quoting=csv.QUOTE_ALL)

        self.stats = csv.DictWriter(open(_stats_path, 'w'), fieldnames=KEYWORDS_STATS_FIELDS,
                                    restval='', extrasaction='ignore',
                                    quotechar='\"', quoting=csv.QUOTE_ALL)

        self.keywords.writeheader()
        self.stats.writeheader()

        self.create_manifest(_keywords_path, incremental, KEYWORDS_PK)
        self.create_manifest(_stats_path, incremental, KEYWORDS_STATS_PK)

    def create_manifest(self, table_path, incremental, primary_key=[]):

        _manifest = {
            'incremental': incremental,
            'primary_key': primary_key
        }

        with open(table_path + '.manifest', 'w') as _man:
            json.dump(_manifest, _man)

    def parse_gad_response(self, response, request_type):

        for _row in response:

            # Convert to dict using a workaround
            row = eval(str(_row))
            data = row['data']

            _kword = None
            kword_dict = {}
            stats_list = []

            for result in data:

                _attr = result['key']
                _value = result['value']['value']

                if _attr == 'AVERAGE_CPC':
                    _value = _value['microAmount'] if _value is not None else None

                if _attr == 'KEYWORD_TEXT':
                    _kword = _value

                if _attr == 'TARGETED_MONTHLY_SEARCHES':
                    stats_list = [{
                        'YEAR': s['year'],
                        'MONTH': s['month'],
                        'COUNT': s['count']
                    } for s in _value]
                    continue

                kword_dict[_attr] = _value

            kword_dict['REQUEST_TYPE'] = request_type

            self.keywords.writerow(kword_dict)
            self.stats.writerows([{**{'KEYWORD_TEXT': _kword, **stat}} for stat in stats_list])
