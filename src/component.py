import csv
import glob
import json
import logging
import os
import sys
import yaml
from pathlib import Path

from client import GoogleAdwordsClient
from result import GoogleAdwordsWriter
from googleads.adwords import AdWordsClient
from kbc.env_handler import KBCEnvHandler

# configuration variables
KEY_DEBUG = 'debug'
KEY_CUSTOMER_IDS = 'customer_ids'
KEY_REQUEST_TYPE = 'request_type'
KEY_REQUEST_ATTRIBUTES = 'requested_attributes'
KEY_NETWORK_SETTINGS = 'network_settings'
KEY_LOCATIONS_SETTINGS = 'locations'
KEY_OTHER_SETTINGS = 'other_parameters'
KEY_INCREMENTAL = 'incremental'
KEY_DEVELOPER_TOKEN = '#developer_token'

MANDATORY_PARS = [KEY_CUSTOMER_IDS, KEY_REQUEST_TYPE]
MANDATORY_IMAGE_PARS = [KEY_DEVELOPER_TOKEN]

SUPPORTED_REQUEST_TYPE = ['STATS', 'IDEAS']
KEYWORD_COLUMN = 'keyword'

APP_VERSION = '0.0.9'


class GoogleAdwordsComponent(KBCEnvHandler):

    def __init__(self, debug=False):
        # for easier local project setup
        default_data_dir = Path(__file__).resolve().parent.parent.joinpath('data').as_posix() \
            if not os.environ.get('KBC_DATADIR') else None

        KBCEnvHandler.__init__(self, MANDATORY_PARS, log_level=logging.DEBUG if debug else logging.INFO,
                               data_path=default_data_dir)

        # override debug from config
        if self.cfg_params.get(KEY_DEBUG) is True:
            logging.getLogger().setLevel(logging.DEBUG)

        logging.info(f'Running version {APP_VERSION}.')

        try:
            self.validate_config(MANDATORY_PARS)
            self.validate_image_parameters(MANDATORY_IMAGE_PARS)
        except ValueError as e:
            logging.exception(e)
            exit(1)

        self.validate_and_get_parameters()
        self.client = GoogleAdwordsClient(AdWordsClient.LoadFromString(yaml.dump({
            'adwords': {
                'developer_token': self.par_developer_token,
                'client_id': self.par_client_id,
                'client_secret': self.par_client_secret,
                'refresh_token': self.par_refresh_token
            }
        })))

        self.writer = GoogleAdwordsWriter(self.tables_out_path, self.par_incremental)

    def _get_developer_token(self):

        try:
            _dev_token = self.configuration.config_data['image_parameters'][KEY_DEVELOPER_TOKEN]
        except KeyError:
            logging.error("Developer token missing in stack parameters.")
            sys.exit(2)

        return _dev_token

    def validate_and_get_parameters(self):

        # Developer token
        self.par_developer_token = self._get_developer_token()

        # Authorization
        _auth = self.get_authorization()
        self.par_client_id = _auth['appKey']
        self.par_client_secret = _auth['#appSecret']
        self.par_refresh_token = json.loads(_auth['#data'])['refresh_token']

        # Validate customer IDs
        self.par_customer_ids = self.cfg_params['customer_ids']
        if len(self.par_customer_ids) == 0:
            logging.error("No customer IDS provided.")
            sys.exit(1)

        # Request type
        self.par_request_type = self.cfg_params[KEY_REQUEST_TYPE]
        if self.par_request_type not in SUPPORTED_REQUEST_TYPE:
            logging.error(f"Unsupported request type {self.par_request_type}. Must be one of {SUPPORTED_REQUEST_TYPE}.")
            sys.exit(1)

        # Locations
        _locations = self.cfg_params.get(KEY_LOCATIONS_SETTINGS, [])
        if _locations == []:
            self.par_locations = None
        else:
            try:
                _locations_object = [{'id': int(loc_id)} for loc_id in _locations]
                self.par_locations = _locations_object
            except ValueError as e:
                logging.error(f"Location IDs must be provided as integers. Provided: {e}.")
                sys.exit(1)

        # Network settings
        _network = self.cfg_params.get(KEY_NETWORK_SETTINGS, {})

        if isinstance(_network, dict):
            if _network == {}:
                self.par_network_settings = None
            else:
                self.par_network_settings = _network

        else:
            if str(_network).strip() == '':
                self.par_network_settings = None
            else:
                logging.error(f"Network settings must be provided as a valid JSON object. Provided value: {_network}.")
                sys.exit(1)

        # Keywords
        _keywords = self.get_keywords()
        if len(_keywords) == 0:
            logging.warn("No keywords provided.")
            sys.exit(0)

        else:
            kw_len = sum([len(_list) for _list in _keywords])
            logging.info(f"{kw_len} keyword(s) provided.")
            logging.debug(f"Keywords: {_keywords}.")
            self.keywords = _keywords

        # Incremental
        self.par_incremental = self.cfg_params.get(KEY_INCREMENTAL, False)

    def get_keywords(self):

        input_tables = glob.glob(os.path.join(self.tables_in_path, '*.csv'))

        if len(input_tables) == 0:
            logging.error("No input tables provided.")
            sys.exit(1)

        all_keywords = []

        for table in input_tables:
            with open(table) as input:
                _rdr = csv.DictReader(input)
                if KEYWORD_COLUMN not in _rdr.fieldnames:
                    logging.error(f"Column \"{KEYWORD_COLUMN}\" is missing in table \"{os.path.basename(table)}\".")
                    sys.exit(1)

                else:
                    for row in _rdr:
                        all_keywords.append(row[KEYWORD_COLUMN].strip())

        keywords_collection_length = 100
        keywords_collections = [all_keywords[i:i+keywords_collection_length] for i in
                                range(0, len(all_keywords), keywords_collection_length)]

        return keywords_collections

    def run(self):

        for customer_id in self.par_customer_ids:

            logging.info(f"Downloading data for customer {customer_id}.")
            self.client.set_customer_id(customer_id)

            for collection in self.keywords:

                _selector = self.client.build_selector(keywords=collection,
                                                       locations=self.par_locations,
                                                       networks=self.par_network_settings,
                                                       request_type=self.par_request_type)
                logging.debug(f"Created selector: {_selector}.")

                keywords_results = self.client.download_keywords(_selector)
                self.writer.parse_gad_response(keywords_results, self.par_request_type)

        logging.info("Extraction finished.")
