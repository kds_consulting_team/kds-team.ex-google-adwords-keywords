import googleads
import logging
import sys
import time


class GoogleAdwordsClient(googleads.adwords.AdWordsClient):

    PAGE_SIZE = 100
    REQUESTED_ATTRIBUTES = ['KEYWORD_TEXT', 'COMPETITION', 'EXTRACTED_FROM_WEBPAGE', 'IDEA_TYPE',
                            'SEARCH_VOLUME', 'AVERAGE_CPC', 'TARGETED_MONTHLY_SEARCHES']
    MAX_RETRIES = 5
    EXPONENTIAL_BACKOFF = [0, 1.1, 1.2, 1.4, 1.7, 2]

    def __init__(self, adwords):

        self.adwords_client = adwords

    def set_customer_id(self, customer_id):

        self.adwords_client.SetClientCustomerId(customer_id)

    def get_customers(self):

        customer_service = self.adwords_client.GetService(service_name='CustomerService')

        try:
            return customer_service.getCustomers()

        except googleads.errors.GoogleAdsServerFault as e:
            logging.error(f"Could not list customers: {e}")
            sys.exit(1)

    def build_selector(self, keywords, locations, networks, request_type):

        selector = dict()
        selector['ideaType'] = 'KEYWORD'
        selector['requestType'] = request_type
        selector['requestedAttributeTypes'] = self.REQUESTED_ATTRIBUTES

        _search_params = []
        _search_params.append({
            'xsi_type': 'RelatedToQuerySearchParameter',
            'queries': keywords
        })

        if locations is not None:
            _search_params.append({
                'xsi_type': 'LocationSearchParameter',
                'locations': locations
            })

        if networks is not None:
            _search_params.append({
                'xsi_type': 'NetworkSearchParameter',
                'networkSetting': networks
            })

        selector['searchParameters'] = _search_params

        return selector

    def _download_keywords_page(self, selector, targeting_service):
        pass

    def download_keywords(self, selector):

        targeting_service = self.adwords_client.GetService(service_name='TargetingIdeaService')
        offset = 0
        is_complete = False
        results = []

        while is_complete is False:

            retry_count = 1

            selector['paging'] = {
                'numberResults': str(self.PAGE_SIZE),
                'startIndex': str(offset)
            }

            try:
                page = targeting_service.get(selector)
                results += (_res := page['entries'])

                if len(_res) < self.PAGE_SIZE:
                    is_complete = True
                else:
                    offset += self.PAGE_SIZE

            except googleads.errors.GoogleAdsServerFault as e:

                _error = e.errors[0]
                _success_repeat = False

                if _error['ApiError.Type'] == 'RateExceededError':

                    _retry_after = int(_error['retryAfterSeconds'])

                    while retry_count < self.MAX_RETRIES + 1 and _success_repeat is False:

                        _wait = _retry_after * self.EXPONENTIAL_BACKOFF[retry_count]
                        logging.warn(f"Rate exceeded. Retrying (attempt #{retry_count}) in {_wait} seconds.")
                        time.sleep(_wait)

                        try:
                            page = targeting_service.get(selector)
                            _success_repeat = True

                        except googleads.errors.GoogleAdsServerFault as e:
                            _error = e.errors[0]
                            _retry_after = int(_error['retryAfterSeconds'])
                            retry_count += 1

                    if _success_repeat is False:
                        logging.error("Could not download keywords ideas/stats - reached max retries.")
                        sys.exit(1)

                else:
                    logging.error(f"An error occurred while downloading data: {e}.")
                    sys.exit(1)

            except Exception as e:
                logging.error(f"An error occurred while downloading data: {e}.")
                sys.exit(1)

        return results
