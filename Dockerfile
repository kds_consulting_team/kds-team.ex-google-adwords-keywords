FROM python:3.8.5-slim
ENV PYTHONIOENCODING utf-8

COPY . /code/

# install gcc to be able to build packages - e.g. required by regex, dateparser, also required for pandas
RUN apt-get update && apt-get install -y build-essential
RUN pip install flake8
RUN pip install --no-cache-dir -r /code/requirements.txt
RUN mkdir -p /var/www && chmod a+rw /var/www -R

WORKDIR /code/

CMD ["python", "-u", "/code/src/run.py"]